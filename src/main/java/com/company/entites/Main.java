package com.company.entites;

import com.company.details.Engine;
import com.company.professions.Driver;
import com.company.vehicles.Car;

public class Main {
    public static void main(String[] args) {

        Car car = new Car();
        Engine engine = new Engine();
        Driver driver = new Driver();
        Person person = new Person();

        car.setCarClass("Sport-Car");
        car.setCarBrand("Toyota");
        car.setWight(500);
        driver.setDrivingExperience(5);
        person.setAge(25);
        person.setGender("Male");
        person.setSurname("Prokopenko");
        person.setPatronymic("Olegovich");
        person.setPhoneNumber(0674563241);

        System.out.println(car.toString());

    }
}
