package com.company.entites;

import com.company.vehicles.Car;

public class SportCar extends Car {
    private int SpeedLimit;

    public int getSpeedLimit() {
        return SpeedLimit;
    }

    public void setSpeedLimit(int speedLimit) {
        SpeedLimit = speedLimit;
    }
}
